Topic modeling
---

**Topic modeling** is a type of statistical modeling for discovering the abstract
“topics” that occur in a collection of documents.
Latent Dirichlet Allocation (LDA) is an example of topic model and is used to
**classify** text in a document to a particular topic.
It builds a topic per document model and words per topic model,
modeled as Dirichlet distributions.


#### Data Pre-processing
###### We will perform the following steps:

 - Tokenization: Split the text into sentences and the sentences into words.
                 Lowercase the words and remove punctuation.
 - Words that have fewer than 3 characters are removed.
 - All stopwords are removed.
 - Words are lemmatized — words in third person are changed to first person
    and verbs in past and future tenses are changed into present.
 - Words are stemmed — words are reduced to their root form.

###### After that got to Bag of Words on the Data set:
 - Create a **dictionary** from ‘processed_docs’
  containing the number of times a word appears in the training set.

###### Filter out tokens that appear in
 - less than 15 documents (absolute number) or
 - more than 0.5 documents (fraction of total corpus size, not absolute number).
 - after the above two steps, keep only the first 100000 most frequent tokens.


###### Gensim doc2bow
    For each document we create a dictionary reporting how many
    words and how many times those words appear. Save this to ‘bow_corpus’, then check our selected document earlier.

LDA
======
    gensim.models.LdaMulticore

TF-IDF
======
  Create tf-idf model object using models.TfidfModel on ‘bow_corpus’ and save it to ‘tfidf’, then apply transformation to the entire corpus and call it ‘corpus_tfidf’. Finally we preview TF-IDF scores for our first document.

