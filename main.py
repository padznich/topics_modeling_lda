from pprint import pprint

import gensim
from gensim import corpora, models
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
import nltk
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.stem.porter import *
import numpy as np
import pandas as pd


data = pd.read_csv('abcnews-date-text.csv', error_bad_lines=False)
data_text = data[['headline_text']]
data_text['index'] = data_text.index
documents = data_text

# # 1-st step: Check data
# print(len(documents))
# print(documents[:5])

# 2-nd step: Download NLTK
nltk.download('wordnet')


def lemmatize_stemming(text):
    return SnowballStemmer('english').stem(WordNetLemmatizer().lemmatize(text, pos='v'))


def preprocess(text):
    result = []
    for token in gensim.utils.simple_preprocess(text):
        if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 3:
            result.append(lemmatize_stemming(token))
    return result


#
# Select a document to preview after preprocessing.
#
doc_sample = documents[documents['index'] == 4310].values[0][0]
print('original document: ')
words = []
for word in doc_sample.split(' '):
    words.append(word)
print(words)
print('\n\n tokenized and lemmatized document: ')
print(preprocess(doc_sample))


#
# 3-d step: Preprocess headline text
# Preprocess the headline text, saving the results as ‘processed_docs’
#
processed_docs = documents['headline_text'].map(preprocess)
print(processed_docs[:10])


#
# Bag of Words on the Data set
#
dictionary = gensim.corpora.Dictionary(processed_docs)
for count, (k, v) in enumerate(dictionary.iteritems()):
    print(k, v)
    count += 1
    if count > 10:
        break


#
#
# Gensim filter_extremes
#
# Filter out tokens that appear in
#
#   less than 15 documents (absolute number) or
#   more than 0.5 documents (fraction of total corpus size, not absolute number).
#   after the above two steps, keep only the first 100000 most frequent tokens.
#
dictionary.filter_extremes(no_below=15, no_above=0.5, keep_n=100000)


#
# Gensim doc2bow
#
# For each document we create a dictionary reporting how many
# words and how many times those words appear. Save this to ‘bow_corpus’, then check our selected document earlier.
#
bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]
print(bow_corpus[4310])


#
# Preview Bag Of Words for our sample preprocessed document.
#
bow_doc_4310 = bow_corpus[4310]
for i in range(len(bow_doc_4310)):
    print("Word {} (\"{}\") appears {} time.".format(bow_doc_4310[i][0],
                                                     dictionary[bow_doc_4310[i][0]],
                                                     bow_doc_4310[i][1]))


#
# TF-IDF
# Create tf-idf model object using models.TfidfModel on ‘bow_corpus’ and save it to ‘tfidf’,
# then apply transformation to the entire corpus and call it ‘corpus_tfidf’.
# Finally we preview TF-IDF scores for our first document.
#
tfidf = models.TfidfModel(bow_corpus)
corpus_tfidf = tfidf[bow_corpus]
for doc in corpus_tfidf:
    pprint(doc)
    break


#
# Running LDA using Bag of Words
# Train our lda model using gensim.models.LdaMulticore and save it to ‘lda_model’
#
# For each topic, we will explore the words occuring in that topic and its relative weight.
#
lda_model = gensim.models.LdaMulticore(bow_corpus, num_topics=10, id2word=dictionary, passes=2, workers=2)
for idx, topic in lda_model.print_topics(-1):
    print('Topic: {} \nWords: {}'.format(idx, topic))


#
# Running LDA using TF-IDF
#
lda_model_tfidf = gensim.models.LdaMulticore(corpus_tfidf, num_topics=10, id2word=dictionary, passes=2, workers=4)
for idx, topic in lda_model_tfidf.print_topics(-1):
    print('Topic: {} Word: {}'.format(idx, topic))


#
# Performance evaluation by classifying sample document using LDA Bag of Words model
# We will check where our test document would be classified.
#
print(processed_docs[4310])
for index, score in sorted(lda_model[bow_corpus[4310]], key=lambda tup: -1*tup[1]):
    print("\nScore: {}\t \nTopic: {}".format(score, lda_model.print_topic(index, 10)))


#
# Testing model on unseen document
#
unseen_document = 'How a Pentagon deal became an identity crisis for Google'
bow_vector = dictionary.doc2bow(preprocess(unseen_document))
for index, score in sorted(lda_model[bow_vector], key=lambda tup: -1*tup[1]):
    print("Score: {}\t Topic: {}".format(score, lda_model.print_topic(index, 5)))
